from django.contrib import admin
from home.models import User

# Register your models here.
class HomeAdmin(admin.ModelAdmin):
    list_display = (
        'username', 
        'password', 
        'client_id', 
        "client_secret", 
        "access_token", 
        "refresh_token", 
        "token_created", 
        "fullname", 
        "npm"
        )

admin.site.register(User, HomeAdmin)