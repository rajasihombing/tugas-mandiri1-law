from rest_framework import serializers
from home.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'username', 
            'password', 
            'client_id', 
            'client_secret'
            )