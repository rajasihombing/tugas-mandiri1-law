import random, string

from home.models import User
from home.serializer import UserSerializer
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from datetime import datetime

# Create your views here.

@api_view(['POST'])
def token(request):
    serializer = UserSerializer(data=request.data)
    if serializer.is_valid():
        data = serializer.data
        if User.objects.filter(username=data["username"]).exists():
            user = User.objects.get(username=data["username"])
            if (user.password == data["password"] and user.client_id == data["client_id"] and user.client_secret == data['client_secret']):
                user.access_token = tokenGenerator()
                if (user.refresh_token == "" or user.refresh_token == None):
                    user.refresh_token = tokenGenerator()
                user.token_created = datetime.now()
                user.save()
                message = {
                    "access_token" : user.access_token,
                    "expires_in" : 300,
                    "token_type" : "Bearer",
                    "scope" : None,
                    "refresh_token" : user.refresh_token, 
                }
                return Response(message, status=status.HTTP_200_OK)
    message = {
        "error":"invalid_request",
        "Error_description":"ada kesalahan masbro!"
    }
    return Response(message, status=status.HTTP_401_UNAUTHORIZED)

@api_view(['POST'])
def resource(request):
    header_auth = request.META.get('HTTP_AUTHORIZATION')
    temp = header_auth.split()
    try:
        if (temp[0] == "Bearer" and User.objects.filter(access_token = temp[1]).exists()):
            user = User.objects.get(access_token = temp[1])

            created = int(user.token_created.strftime("%Y%m%d%H%M%S"))
            now = int(datetime.now().strftime("%Y%m%d%H%M%S"))

            if(now-created >= 0 and now-created <= 300):
                message = {
                    "access_token" : user.access_token,
                    "client_id" : user.client_id,
                    "user_id" : user.username,
                    "full_name" : user.fullname,
                    "npm" : user.npm,
                    "expires" : None,
                    "refresh_token" : user.refresh_token
                }
                return Response(message, status=status.HTTP_200_OK)
            else:
                raise Exception
    except:
        pass
    message = {
        "error" : "invalid_token",
        "error_description" : "Token Salah masbro",
    }
    return Response(message, status=status.HTTP_401_UNAUTHORIZED)



def tokenGenerator():
    token = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for i in range(40))
    return str(token)